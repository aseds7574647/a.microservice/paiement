const axios = require('axios');
const amqp = require('amqplib');

exports.payerUneCommande = async (req, res) => {
    const order = req.body;
    try {
        // Publier un message pour le service « email »
        const connection = await amqp.connect(`amqp://${process.env.RABBITMQ_HOST}`);
        const channel = await connection.createChannel();

        const queue = process.env.RABBITMQ_QUEUE;
        await channel.assertQueue(queue, { durable: true });

        channel.sendToQueue(queue, Buffer.from(JSON.stringify(order.orderId)), { persistent: true });
        // Met à jour le statut de la commande en appelant le microservice de commande
        await axios.put(`http://host.docker.internal:3006/commande/${order.orderId}`, {commandePayee: true})
            .then(products => {
                console.log("paiement effectué");
            })
            .catch(error => {
                console.error(error);
            })
        console.log(`Confirmation email request sent for payment of command: ${order.orderId}`);
        res.status(201).json("Paiement effectue");
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};