const express = require('express');
const router = express.Router();
const paiementController = require('../controllers/stuff');

router.post('/', paiementController.payerUneCommande);

module.exports = router;