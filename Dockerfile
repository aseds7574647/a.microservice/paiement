# Utilise l'image Node.js 14
FROM node:16

# Définit le répertoire de travail à /Mcommerce
WORKDIR /Mcommerce/paiement

# Copie les fichiers du microservice dans le conteneur à /Mcommerce/paiement
COPY . /Mcommerce/paiement/

# Installe les dépendances du projet
RUN npm install

# Expose le port 3001 du conteneur (ou le port que votre microservice utilise)
EXPOSE 3007

# Ajouter la configuration RabbitMQ
ENV RABBITMQ_HOST=rabbitmq
ENV RABBITMQ_QUEUE=confirmationEmailQueue

# Commande à exécuter lors du démarrage du conteneur
CMD ["node", "serverPaiement.js"]

